#pragma once
#include "testbaseclass.h"
#include <string>


class TestClass :
	public TestBaseClass
{
public:
	TestClass(void);
	~TestClass(void);
	void call(std::string s);
};
